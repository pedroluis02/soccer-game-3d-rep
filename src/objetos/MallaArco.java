/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import javax.media.j3d.Appearance;
import javax.media.j3d.PointArray;
import javax.media.j3d.PointAttributes;
import javax.media.j3d.Shape3D;
import javax.media.j3d.TransformGroup;
import uti.Metodosj3d;

/**
 *
 * @author Pedro Luis
 */
public class MallaArco extends TransformGroup
{
    private PointArray mallaArriba, mallaAtras;
    private PointArray mallaIzquierda, mallaDerecha;
    private Appearance appMalla;
    private double X, Y, Y_2, Z; 
    private int tipo;
    public MallaArco(double x, double y, double z, int tipo)
    {
        this.tipo = tipo;
        if(tipo==2)
           X = -x;
        else
           X = x;
        Y = y; Y_2 = Y/2; Z = z;
        
        appMalla = new Appearance();
        PointAttributes laMalla = new PointAttributes();
        laMalla.setPointSize(1.f);
        appMalla.setPointAttributes(laMalla);
        
        crearMallaArriba();
        crearMallaDer();
        crearMallaIzq();
        crearMallaAtras();
        
        addChild(new Shape3D(mallaArriba, appMalla));
        addChild(new Shape3D(mallaDerecha, appMalla));
        addChild(new Shape3D(mallaIzquierda, appMalla));
        addChild(new Shape3D(mallaAtras, appMalla));
    }
    private void crearMallaArriba()
    {
        double[][] pXA = null;
        double X1 = 0.0;
        if(tipo==1)
        {
           X1 = X-0.8;
         pXA = new double[][]{{X1, X1, X1, X1},
         {X1+0.3, X1+0.3, X1+0.3, X1+0.3},
         {X1+0.5, X1+0.5, X1+0.5, X1+0.5},  
         {X1+0.8, X1+0.8, X1+0.8, X1+0.8}};
        }
        else
        {
           X1 = X+0.8;
         pXA = new double[][]{{X1, X1, X1, X1},
         {X1-0.3, X1-0.3, X1-0.3, X1-0.3},
         {X1-0.5, X1-0.5, X1-0.5, X1-0.5},  
         {X1-0.8, X1-0.8, X1-0.8, X1-0.8}};
        }

        double[][] pYA = 
        {{-Y, -Y_2, Y_2, Y}, 
         {-Y, -Y_2, Y_2, Y},
         {-Y, -Y_2, Y_2, Y},
         {-Y, -Y_2, Y_2, Y}};
        
        //System.out.println("Z1:"+ Z);
        double[][] pZA = {{Z+0.2, Z+0.2, Z+0.2, Z+0.2},
                          {Z+0.15, Z+0.3, Z+0.3, Z+0.15},
                          {Z+0.05, Z+0.3, Z+0.3, Z+0.05},
                          {Z, Z, Z, Z}};
        
        mallaArriba = Metodosj3d.surfaceBezier(pXA, pYA, pZA);
    }
    private void crearMallaDer()
    {
        double[][] pDX = null;
        double X1 = 0.0;
        if(tipo==1)
        {
           X1 = X-0.8;
         pDX = new double[][]{{X1, X1, X1, X1},
         {X1+0.3, X1+0.3, X1+0.3, X1+0.3},
         {X1+0.5, X1+0.5, X1+0.5, X1+0.5},  
         {X1+0.8, X1+0.8, X1+0.8, X1+0.8}};
        }
        else
        {
           X1 = X+0.8;
         pDX = new double[][]{{X1, X1, X1, X1},
         {X1-0.3, X1-0.3, X1-0.3, X1-0.3},
         {X1-0.5, X1-0.5, X1-0.5, X1-0.5},  
         {X1-0.8, X1-0.8, X1-0.8, X1-0.8}};
        }
        
        double[][] pDY = 
        {{Y, Y, Y, Y}, 
         {Y, Y, Y, Y},
         {Y, Y, Y, Y},
         {Y, Y, Y, Y}};
        
        double[][] pDZ = 
        {{Z+0.2, Z, Z-0.2, 0},
         {Z+0.15, Z-0.02, Z-0.22, 0},
         {Z+0.05, Z-0.1, Z-0.26, 0},
         {Z, Z-0.1, Z-0.3, 0}};
         
        mallaDerecha = Metodosj3d.surfaceBezier(pDX, pDY, pDZ);
    }
    private void crearMallaIzq()
    {
        double[][] pIX = null;
        double X1 = 0.0;
        if(tipo==1)
        {
           X1 = X-0.8;
         pIX = new double[][]{{X1, X1, X1, X1},
         {X1+0.3, X1+0.3, X1+0.3, X1+0.3},
         {X1+0.5, X1+0.5, X1+0.5, X1+0.5},  
         {X1+0.8, X1+0.8, X1+0.8, X1+0.8}};
        }
        else
        {
           X1 = X+0.8;
         pIX = new double[][]{{X1, X1, X1, X1},
         {X1-0.3, X1-0.3, X1-0.3, X1-0.3},
         {X1-0.5, X1-0.5, X1-0.5, X1-0.5},  
         {X1-0.8, X1-0.8, X1-0.8, X1-0.8}};
        }
        
        double[][] pIY = 
        {{-Y, -Y, -Y, -Y}, 
         {-Y, -Y, -Y, -Y},
         {-Y, -Y, -Y, -Y},
         {-Y, -Y, -Y, -Y}};
        
        double[][] pIZ = 
        {{Z+0.2, Z, Z-0.2, 0},
         {Z+0.15, Z-0.02, Z-0.22, 0},
         {Z+0.05, Z-0.1, Z-0.26, 0},
         {Z, Z-0.1, Z-0.3, 0}};
         
        mallaIzquierda = Metodosj3d.surfaceBezier(pIX, pIY, pIZ);
    }
    private void crearMallaAtras()
    {
        double eM = 0.0;
        if(tipo==1)
           eM = X + 0.4;
        else
            eM = X - 0.4;
        double[][] pXA = 
        {{X, X, X, X},
         {X, eM, eM, X},  
         {X, eM, eM, X}, 
         {X, X, X, X}};
        
        double[][] pYA = 
        {{-Y, -Y_2, Y_2, Y}, 
         {-Y, -Y_2, Y_2, Y},
         {-Y, -Y_2, Y_2, Y},
         {-Y, -Y_2, Y_2, Y}};
        
        double[][] pZA = 
        {{Z, Z, Z, Z},
         {Z-0.1, Z-0.1, Z-0.1, Z-0.1},
         {Z-0.3, Z-0.3, Z-0.3, Z-0.3},
         {0, 0, 0, 0}};
        
        mallaAtras = Metodosj3d.surfaceBezier(pXA, pYA, pZA);
    }
}
