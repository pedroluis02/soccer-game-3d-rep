/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.geometry.Sphere;
import com.sun.j3d.utils.image.TextureLoader;
import java.net.URL;
import javax.media.j3d.Appearance;
import javax.media.j3d.Texture;
import javax.media.j3d.TextureAttributes;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Vector3d;

/**
 *
 * @author Pedro Luis
 */
public class Balon extends TransformGroup
{
    private Transform3D t3dBalon;
    private Sphere balon;
    private Appearance appBalon;
    public Balon(float radio)
    {
        t3dBalon = new Transform3D();
        t3dBalon.setTranslation(new Vector3d(0, 0, 0.1));
        appBalon = new Appearance();
        setTransform(t3dBalon);
        TextureAttributes taBalon = new TextureAttributes();
        taBalon.setTextureMode(TextureAttributes.MODULATE);
        appBalon.setTexture(cargarTextura("pelota.jpg"));
        appBalon.setTextureAttributes(taBalon);
        balon = new Sphere(radio, Primitive.GENERATE_TEXTURE_COORDS, appBalon);
        setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        addChild(balon);
    }
    private Texture cargarTextura(String textura)
    {
        URL url = getClass().getResource("/images/"+textura);
        TextureLoader loader = new TextureLoader(url, null);
        return loader.getTexture();
    }
    public void mover(double x, double y, double z)
    {
        Transform3D td = new Transform3D();
        getTransform(td);
        td.setTranslation(new Vector3d(x, y, z));
        setTransform(td);
    }
}
