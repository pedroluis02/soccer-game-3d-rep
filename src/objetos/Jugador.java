/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Cylinder;
import com.sun.j3d.utils.geometry.Sphere;
import java.awt.Color;
import java.awt.Font;
import javax.media.j3d.Appearance;
import javax.media.j3d.ColoringAttributes;
import javax.media.j3d.Font3D;
import javax.media.j3d.FontExtrusion;
import javax.media.j3d.PointArray;
import javax.media.j3d.PointAttributes;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Text3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3d;
import uti.Metodosj3d;

/**
 *
 * @author Pedro Luis
 */
public class Jugador  extends TransformGroup
{
    private TransformGroup jCabeza;
    private TransformGroup jOjo1;
    private TransformGroup jOjo2;
    private TransformGroup jHombros;
    private TransformGroup jCuerpo;
    private TransformGroup jBrazo1;
    private TransformGroup jBrazo2;
    private TransformGroup jGroup;
    private TransformGroup jPierna1;
    private TransformGroup jPierna2;
    ///
    private TransformGroup jNombre;
    private TransformGroup jNumero;
    //
    private Appearance colorPiel;
    //
    private double X, Y, Z;
    public Jugador(Color3f ca, Color3f pol, Color3f sh, String nom, int n)
    {
       colorPiel = new Appearance();
       colorPiel.setColoringAttributes(
               new ColoringAttributes(new Color3f(Color.pink), ColoringAttributes.SHADE_FLAT));
       
       Transform3D td = new Transform3D();
       td.setScale(0.08);
       setTransform(td);
       setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
       setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
       crearCabeza();
       crearCabello(ca);
       crearOjos();
       createHombros(pol);
       crearCuerpo(pol);
       crearBrazos();
       crearGrupo(sh);
       crearPiernas();
       
       crearProp(nom, n);
    }
    private void crearCabeza()
    {
        Transform3D t3dc = new Transform3D();
        t3dc.setTranslation(new Vector3d(0, 0, 2));
        jCabeza = new TransformGroup(t3dc);
        
        Sphere cabeza = new Sphere(1.f, colorPiel);
        jCabeza.addChild(cabeza);
        addChild(jCabeza);
    }
    private void crearOjos()
    {
        Transform3D tdo1 = new Transform3D();
        tdo1.setTranslation(new Vector3d(0.7f, 0.4, 2.3));
        jOjo1 = new TransformGroup(tdo1);
        Sphere ojo1 = new Sphere(0.2f);
        jOjo1.addChild(ojo1);
        
        Transform3D tdo2 = new Transform3D();
        tdo2.setTranslation(new Vector3d(0.7f, -0.4, 2.3));
        jOjo2 = new TransformGroup(tdo2);
        Sphere ojo2 = new Sphere(0.2f);
        jOjo2.addChild(ojo2);
        
        addChild(jOjo1);
        addChild(jOjo2);
    }
    private void crearCuerpo(Color3f colorC)
    {
        Appearance appCa = new Appearance();
        appCa.setColoringAttributes(
                new ColoringAttributes(colorC, ColoringAttributes.NICEST));
        Transform3D t3dc = new Transform3D();
        t3dc.setRotation(new AxisAngle4d(0, 1, 1, 160));
        jCuerpo = new TransformGroup(t3dc);
        
        Cylinder cuerpo = new Cylinder(1.0f, 2.0f, appCa);
        Cylinder cuello = new Cylinder(0.5f, 3.0f);
        
        jCuerpo.addChild(cuerpo);
        jCuerpo.addChild(cuello);
        
        addChild(jCuerpo);
    }
    private void crearGrupo(Color3f colorG)
    {
        Appearance appG = new Appearance();
        appG.setColoringAttributes(
                new ColoringAttributes(colorG, ColoringAttributes.NICEST));
        Transform3D tdg = new Transform3D();
        tdg.setTranslation(new Vector3d(0.2, 0, -1));
        jGroup = new TransformGroup(tdg);
        
        //Bix(x, y, z)
        Box group = new Box(0.55f, 0.8f, 1.0f, appG);
        jGroup.addChild(group);
        
        addChild(jGroup);
    }
    private void createHombros(Color3f colorH)
    {
        Appearance appHO = new Appearance();
        appHO.setColoringAttributes(
                new ColoringAttributes(colorH, ColoringAttributes.NICEST));
        Transform3D t3dh = new Transform3D();
        t3dh.setTranslation(new Vector3d(0, 0, 0.4f));
        jHombros = new TransformGroup(t3dh);
        Cylinder hombros = new Cylinder(0.5f, 2.5f, appHO); 
        jHombros.addChild(hombros);
        
        addChild(jHombros);
    }
    private void crearCabello(Color3f colorC)
    {
        Appearance appCa = new Appearance();
        appCa.setColoringAttributes(
                new ColoringAttributes(colorC, ColoringAttributes.NICEST));
        PointAttributes pa = new PointAttributes();
        pa.setPointSize(1.5f);
        appCa.setPointAttributes(pa);
        
        double[][] px = new double[][]{{-0.7, -0.7, -0.7, -0.7},
            {-0.7, -1.4, -1.4, -0.7},
            {-0.7, -1.2, -1.2, -0.7},
            {-0.4, -0.6, -0.6, -0.4}};
        double[][] py = new double[][]{{-0.5, -0.5, 0.5, 0.5},
            {-0.5, -0.5, 0.5, 0.5},
            {-0.5, -0.5, 0.5, 0.5},
            {-0.5, -0.5, 0.5, 0.5}};
        double[][] pz = new double[][]{{1.5, 1.5, 1.5, 1.5},
            {1.8, 1.8, 1.8, 1.8},
            {2.2, 2.2, 2.2, 2.2},
            {3, 3, 3, 3}};
        
        PointArray pCabelloAtras = Metodosj3d.surfaceBezier(px, py, pz);
        addChild(new Shape3D(pCabelloAtras, appCa));
        
        //parte de arriba
        double[][] pxA = new double[][]{{-0.6, -0.6, -0.6, -0.6},
            {-0.5, -0.5, -0.5, -0.5},
            {0.5, 0.5, 0.5, 0.5},
            {0.8, 0.8, 0.8, 0.8}};
        double[][] pyA = new double[][]{{-0.8, -0.8, 0.8, 0.8},
            {-0.8, -0.6, 0.6, 0.8},
            {-0.8, -0.6, 0.6, 0.8},
            {-0.6, -0.6, 0.6, 0.6}};
        double[][] pzA = new double[][]{{2.5, 2.5, 2.5, 2.5},
            {2.5, 3.8, 3.8, 2.5},
            {2.5, 3.5, 3.5, 2.5},
            {2.5, 2.5, 2.5, 2.5}};
        
        PointArray pCabelloArriba = Metodosj3d.surfaceBezier(pxA, pyA, pzA);
        addChild(new Shape3D(pCabelloArriba, appCa));
    }
    private void crearBrazos()
    {
        Transform3D tdb1 = new Transform3D();
        tdb1.setTranslation(new Vector3d(0.5, -1.4, -0.2));
        tdb1.setRotation(new AxisAngle4d(0, 1, 1, 90));
        jBrazo1 = new TransformGroup(tdb1);
        Cylinder b1 = new Cylinder(0.25f, 2.0f, colorPiel);  
        jBrazo1.addChild(b1);
        
        Transform3D tdb2 = new Transform3D();
        tdb2.setTranslation(new Vector3d(0.5, 1.4, -0.2));
        tdb2.setRotation(new AxisAngle4d(0, -1, 1, -90));
        jBrazo2 = new TransformGroup(tdb2);
        Cylinder b2 = new Cylinder(0.25f, 2.0f, colorPiel);  
        jBrazo2.addChild(b2);
        
        addChild(jBrazo1);
        addChild(jBrazo2);
    }
    private void crearPiernas()
    {
        Transform3D tdp1 = new Transform3D();
        tdp1.setTranslation(new Vector3d(0.2, 0.4, -2));
        tdp1.setRotation(new AxisAngle4d(0, 1, 1, 160));
        jPierna1 = new TransformGroup(tdp1);
        Cylinder p1 = new Cylinder(0.35f, 4.0f, colorPiel);  
        jPierna1.addChild(p1);
        
        Transform3D tdp2 = new Transform3D();
        tdp2.setTranslation(new Vector3d(0.2, -0.4, -2));
        tdp2.setRotation(new AxisAngle4d(0, 1, 1, 160));
        jPierna2 = new TransformGroup(tdp2);
        Cylinder p2 = new Cylinder(0.35f, 4.0f, colorPiel);
        jPierna2.addChild(p2);
        
        addChild(jPierna1);
        addChild(jPierna2);
    }
    private void crearProp(String nombre, int numero)
    {
        Font3D f3 = new Font3D(new Font(Font.DIALOG, Font.BOLD, 2), new FontExtrusion());
      
        Transform3D t3nu = new Transform3D();
        t3nu.setTranslation(new Vector3d(-0.2, 0, 3.5));
        t3nu.setRotation(new AxisAngle4d(0, 1, 1, -60));
        jNumero = new TransformGroup(t3nu);
        Text3D t3NU = new Text3D(f3);
        t3NU.setString(""+numero);

        jNumero.addChild(new Shape3D(t3NU));
        
        addChild(jNumero);
    }
    public void mover(double x, double y, double z)
    {
        X = x; Y = y; Z = z;
        Transform3D t3 = new Transform3D();
        getTransform(t3);
        t3.setTranslation(new Vector3d(x, y, z));
        setTransform(t3);
    }
    public void rotar(double x, double y, double z, double a)
    {
        Transform3D t3 = new Transform3D();
        getTransform(t3);
        t3.setRotation(new AxisAngle4d(x, y, z, a));
        setTransform(t3);
    }
    public double getX()
    {
        return X;
    }
    public double getY()
    {
        return Y;
    }
    public double getZ()
    {
        return Z;
    }
}
