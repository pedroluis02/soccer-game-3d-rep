/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.awt.Color;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Color3f;

/**
 *
 * @author Pedro Luis
 */
public class Equipo
{
    private Jugador jugadores[];
    private Jugador arquero;
    private Color3f cab, cam, sh;
    private int numero;
    private int tipo;
    public double X=5, Y=3, Z_T = 0.25;
    public Equipo(TransformGroup tg, Color3f cam, Color3f sh, int numero, int tipo)
    {
        this.cam = cam;
        this.sh = sh;
        this.tipo = tipo;
        cab = new Color3f(Color.black);
        this.numero = numero;
        jugadores = new Jugador[10];
        //Color3f car = new Color3f(Color.yellow);
        arquero = new Jugador(cab, cam, sh, "p"+(numero+1), numero+1);
        crearJugadores(tg);
    }
    private void crearJugadores(TransformGroup tg)
    {
        for(int i=0; i<numero; i++)
        {
            jugadores[i] = new Jugador(cab, cam, sh, "p"+(i+1), i+1);
            ubicarJugador(i);
            tg.addChild(jugadores[i]);
        }
        ubicarArquero(tg);
    }
    private void ubicarJugador(int num)
    {
        double p = 1;
        if(tipo==2)
           p=-1;
        switch(num)
        {
            case 0: jugadores[num].mover(p*(X+0.5), 0, Z_T);
                    break;
            case 1: jugadores[num].mover(p*X, Y, Z_T);
                    break;
            case 2: jugadores[num].mover(p*X, -Y, Z_T);
                    break;
            case 3: jugadores[num].mover(p*(X-1), Y-1, Z_T);
                    break;
            case 4: jugadores[num].mover(p*(X-1), -(Y-1), Z_T);
                    break;
            case 5: jugadores[num].mover(p*(X-2.5), Y, Z_T);
                    break;
            case 6: jugadores[num].mover(p*(X-2), 0, Z_T);
                    break;
            case 7: jugadores[num].mover(p*(X-2.5), -Y, Z_T);
                    break;
            case 8: if(tipo==1)
                       jugadores[num].mover(0, Y-2.5, Z_T);
                    else
                       jugadores[num].mover(p*(X-3.5), Y-1.5, Z_T);
                    break;
            case 9: if(tipo==1)
                       jugadores[num].mover(0, -(Y-2.5), Z_T);

                    else
                        jugadores[num].mover(p*(X-3.5), -(Y-1.5), Z_T);
                    break;
        }
        if(tipo==1)
        {
            if(num==8)
               jugadores[num].rotar(0, 0, 1, -90);
            else if(num==9)
               jugadores[num].rotar(0, 0, 1, 90);
            else
               jugadores[num].rotar(0, 0, 1, 60);
        }
    }
    private void ubicarArquero(TransformGroup tg)
    {
        double p=1;
        if(tipo==2)
           p=-1;
        arquero.mover(p*(X+2.8), 0, Z_T);
        if(tipo==1)
           arquero.rotar(0, 0, 1, 60);
        tg.addChild(arquero);
    }
    ////////////
    public Jugador getJugador(int index)
    {
        return jugadores[index];
    }
}
