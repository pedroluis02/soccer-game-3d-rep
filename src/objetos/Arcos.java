/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.awt.Color;
import javax.media.j3d.Appearance;
import javax.media.j3d.ColoringAttributes;
import javax.media.j3d.LineArray;
import javax.media.j3d.LineAttributes;
import javax.media.j3d.Shape3D;
import javax.media.j3d.TransformGroup; 
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;

/**
 *
 * @author Pedro Luis
 */
public class Arcos extends TransformGroup
{
    private LineArray traFrente1;
    private LineArray traFrente2;
    private Appearance appArco;
    private MallaArco malla1;
    private MallaArco malla2;
    private final double X, Y, Z_I=0, Z_S; 
    private final double XA, ZA_S;
    public Arcos(double x, double y)
    {
        X=x; Y = y; Z_S = y;
        XA = X+0.8; ZA_S = Z_S-0.2;
        appArco = new Appearance();
        appArco.setColoringAttributes(new ColoringAttributes(
                new Color3f(Color.white), ColoringAttributes.SHADE_FLAT));
        LineAttributes la = new LineAttributes();
        la.setLineWidth(5.0f); 
        appArco.setLineAttributes(la);
        
        traFrente1 = new LineArray(16, LineArray.COORDINATES);
        traFrente2 = new LineArray(16, LineArray.COORDINATES);
        
        malla1 = new MallaArco(XA, Y, ZA_S, 1);
        malla2 = new MallaArco(XA, Y, ZA_S, 2);
        
        crearArco1();
        crearArco2();
        
        addChild(malla1);
        addChild(malla2);
    }
    private void crearArco1()
    {
        //Arriba
        traFrente1.setCoordinate(0, new Point3d(X, Y, Z_S));
        traFrente1.setCoordinate(1, new Point3d(X, -Y, Z_S));
        //Izq
        traFrente1.setCoordinate(2, new Point3d(X, -Y, Z_I));
        traFrente1.setCoordinate(3, new Point3d(X, -Y, Z_S));
        //Der
        traFrente1.setCoordinate(4, new Point3d(X, Y, Z_I));
        traFrente1.setCoordinate(5, new Point3d(X, Y, Z_S));

        //Atras izq
        traFrente1.setCoordinate(6, new Point3d(XA, Y, Z_I));
        traFrente1.setCoordinate(7, new Point3d(XA, Y, ZA_S));
        //Atras Der
        traFrente1.setCoordinate(8, new Point3d(XA, -Y, Z_I));
        traFrente1.setCoordinate(9, new Point3d(XA, -Y, ZA_S));
        
        //Atras Sup Izq
        traFrente1.setCoordinate(10, new Point3d(XA, Y, ZA_S));
        traFrente1.setCoordinate(11, new Point3d(X, Y, Z_S));
        //Atras Sup Der
        traFrente1.setCoordinate(12, new Point3d(XA, -Y, ZA_S));
        traFrente1.setCoordinate(13, new Point3d(X, -Y, Z_S));
        //Atras Sup Arriba
        traFrente1.setCoordinate(14, new Point3d(XA, -Y, ZA_S));
        traFrente1.setCoordinate(15, new Point3d(XA, Y,  ZA_S));
        //System.out.println(XA);
        
        addChild(new Shape3D(traFrente1, appArco));
    }
    private void crearArco2()
    {
        //Arriba
        traFrente2.setCoordinate(0, new Point3d(-X, Y, Z_S));
        traFrente2.setCoordinate(1, new Point3d(-X, -Y, Z_S));
        //Izq
        traFrente2.setCoordinate(2, new Point3d(-X, -Y, Z_I));
        traFrente2.setCoordinate(3, new Point3d(-X, -Y, Z_S));
        //Der
        traFrente2.setCoordinate(4, new Point3d(-X, Y, Z_I));
        traFrente2.setCoordinate(5, new Point3d(-X, Y, Z_S));
        
        //Atras izq
        traFrente2.setCoordinate(6, new Point3d(-XA, Y, Z_I));
        traFrente2.setCoordinate(7, new Point3d(-XA, Y, ZA_S));
        //Atras Der
        traFrente2.setCoordinate(8, new Point3d(-XA, -Y, Z_I));
        traFrente2.setCoordinate(9, new Point3d(-XA, -Y, ZA_S));
        
        //Atras Sup Izq
        traFrente2.setCoordinate(10, new Point3d(-XA, Y, ZA_S));
        traFrente2.setCoordinate(11, new Point3d(-X, Y, Z_S));
        //Atras Sup Der
        traFrente2.setCoordinate(12, new Point3d(-XA, -Y, ZA_S));
        traFrente2.setCoordinate(13, new Point3d(-X, -Y, Z_S));
        //Atras Sup Arriba
        traFrente2.setCoordinate(14, new Point3d(-XA, -Y, ZA_S));
        traFrente2.setCoordinate(15, new Point3d(-XA, Y,  ZA_S));
        
        addChild(new Shape3D(traFrente2, appArco));
    }
}
