/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.util.Enumeration;
import javax.media.j3d.Alpha;
import javax.media.j3d.Behavior;
import javax.media.j3d.Morph;
import javax.media.j3d.Transform3D;
import javax.media.j3d.WakeupOnElapsedFrames;

/**
 *
 * @author Pedro Luis
 */
public class Jugada extends Behavior
{
    private Equipo equipo;
    private Balon balon;
    private Alpha alpha;
    private int value[];
    private double valueC[];
    private int numero;
    private int dato;
    private int it;
    private double xY;
    WakeupOnElapsedFrames w = new WakeupOnElapsedFrames(0);
    public Jugada(Equipo equipo, Balon  balon)
    {
        this.equipo = equipo;
        this.balon = balon;
        alpha = new Alpha(-1, 1000);
        alpha.setStartTime(System.currentTimeMillis());
        numero = 1;
        it = 0;
    }
    private void jugada1()
    {
       if(dato==8)
       {
           equipo.getJugador(8).mover(equipo.getJugador(8).getX(), valueC[it], equipo.Z_T);
           valueC[it] -=0.01;
           if(valueC[it]<0.2)
              dato = value[++it];
       }
       else if(dato==7)
       {
           equipo.getJugador(7).mover(valueC[it], equipo.getJugador(7).getY(), equipo.Z_T);
           valueC[it] -=0.01;
           if(valueC[it]<0.2)
              dato = value[++it];
       }
       else if(dato==9)
       {
           equipo.getJugador(9).mover(valueC[it], equipo.getJugador(9).getY(), equipo.Z_T);
           valueC[it] -=0.01;
           if(valueC[it]<-4)
              dato = value[++it];
       }
       else if(dato==-1)
       {
           if(it==1 )
           {
               balon.mover(0, valueC[it], 0);
               valueC[it] -=0.01;
               if(valueC[it]<0.2)
               {
                  dato = value[++it];
               }
           }
           else if(it==2)
           {
               balon.mover(0, valueC[it], 0);
               valueC[it] -=0.02;
               if(valueC[it]<-3)
               {
                  dato = value[++it];
               }
           }
           else if(it==5)
           {
               balon.mover(xY, valueC[it], 0);
               valueC[it] +=0.02;
               xY -= 0.03;
               if(xY<-4)
               {
                  dato = value[++it];
                  xY = 0;
               }
           }
           else if(it==6)
           {
               balon.mover(valueC[it], -0.5, 0);
               valueC[it] -=0.03; 
               if(valueC[it]<-8.2)
               {
                  it = 0;
                  dato = value[it];
                  valueC = new double[]{equipo.getJugador(8).getY(), 0, 0, equipo.getJugador(7).getX(), 
                  equipo.getJugador(9).getY(), -3, -4};
               }
           }
       }
    }
    private void jugada2()
    {
        
    }
    @Override
    public void initialize() 
    {
        value = new int[]{8, -1, -1, 7, 9, -1, -1};
        valueC = new double[]{equipo.getJugador(8).getY(), 0, 0, equipo.getJugador(7).getX(), 
        equipo.getJugador(9).getY(), -3, -4};
        valueC = new double[]{equipo.getJugador(8).getY(), 0, 0, equipo.getJugador(7).getX(), 
                  equipo.getJugador(9).getY(), -3, -4};
        dato = value[it];
        xY = 0;
        wakeupOn(w);
    }

    @Override
    public void processStimulus(Enumeration enmrtn)
    {
        if(numero==1)
           jugada1();
         else
           jugada2();
        wakeupOn(w);
    }
    
}
