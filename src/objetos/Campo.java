/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.image.TextureLoader;

import javax.media.j3d.Appearance;
import javax.media.j3d.ColoringAttributes;
import javax.media.j3d.LineArray;
import javax.media.j3d.LineAttributes;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Texture;
import javax.media.j3d.TextureAttributes;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;

import java.net.URL;
import javax.media.j3d.PointArray;
import javax.media.j3d.Transform3D;
import javax.vecmath.Vector3d;
import uti.Metodosj3d;

/**
 *
 * @author Pedro Luis
 */
public class Campo  extends TransformGroup
{
   private Box boxCampo;
   private Appearance appCampo;
   private Appearance appL;
   private LineArray liCampo;
   private Arcos arcos;
   private PointArray centro, semiCentro1, semiCentro2;
   private final double YC = 0.6;
   private final double X = 8, Y = 4;
   private final double AX = X-X/3, AY = Y/2;
   private final double AX_M = X-AX/4, AY_M = (AY + YC)/2;
   
   public Campo()
   {
        Transform3D t3dCampo = new Transform3D();
        t3dCampo.setTranslation(new Vector3d(0, -0.2, -0.15));
              
        TextureAttributes taCampo = new TextureAttributes();
        taCampo.setTextureMode(TextureAttributes.MODULATE);
        appL = new Appearance();
        LineAttributes la = new LineAttributes();
        la.setLineWidth(2.0f); appL.setLineAttributes(la);
        appCampo = new Appearance();
        appCampo.setTexture(cargarTextura("campo.jpg"));
        appCampo.setTextureAttributes(taCampo);
        appCampo.setColoringAttributes(new ColoringAttributes(new 
                Color3f(0.f, 0.5f, 0.25f), ColoringAttributes.NICEST));
        boxCampo = new Box((float)(X+1), (float)(Y+1), 0.1f, Primitive.GENERATE_TEXTURE_COORDS,
                appCampo);
        
        TransformGroup tgCampo = new TransformGroup(t3dCampo);
        tgCampo.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        tgCampo.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        
        tgCampo.addChild(boxCampo);
        
        arcos = new Arcos(X, YC);
        centro = Metodosj3d.calcularPuntosCirculo(0, 0, 0, 0.8, Metodosj3d.TODO);
        semiCentro1 = Metodosj3d.calcularPuntosCirculo(AX, 0, 0, 0.5, Metodosj3d.SUPERIOR);
        semiCentro2 = Metodosj3d.calcularPuntosCirculo(-AX, 0, 0, 0.5, Metodosj3d.INFERIOR);
        
        addChild(tgCampo);
        addChild(lineasBordeCampo());
        addChild(arcos);
        addChild(new Shape3D(centro, appL));
        addChild(new Shape3D(semiCentro1, appL));
        addChild(new Shape3D(semiCentro2, appL));
   }
   private Shape3D lineasBordeCampo()
   {
        liCampo = new LineArray(34, LineArray.COORDINATES);
        //ancho 2
        liCampo.setCoordinate(0, new Point3d(-X, -Y, 0));
        liCampo.setCoordinate(1, new Point3d(-X, Y, 0));
        //largo 2
        liCampo.setCoordinate(2, new Point3d(-X, Y, 0));
        liCampo.setCoordinate(3, new Point3d(X, Y, 0));
        //ancho 1
        liCampo.setCoordinate(4, new Point3d(X, Y, 0));
        liCampo.setCoordinate(5, new Point3d(X, -Y, 0));
        //largo 1
        liCampo.setCoordinate(6, new Point3d(X, -Y, 0));
        liCampo.setCoordinate(7, new Point3d(-X, -Y, 0));
        //centro
        liCampo.setCoordinate(8, new Point3d(0, -Y, 0));
        liCampo.setCoordinate(9, new Point3d(0, Y, 0));
        
        
        /*area1*/
        //linea izq
        liCampo.setCoordinate(10, new Point3d(X, AY, 0));
        liCampo.setCoordinate(11, new Point3d(AX, AY, 0));
        //linea der
        liCampo.setCoordinate(12, new Point3d(X, -AY, 0));
        liCampo.setCoordinate(13, new Point3d(AX, -AY, 0));
        //liea arriba
        liCampo.setCoordinate(14, new Point3d(AX, AY, 0));
        liCampo.setCoordinate(15, new Point3d(AX, -AY, 0));
        
        /*area2*/
        //linea izq
        liCampo.setCoordinate(16, new Point3d(-X, AY, 0));
        liCampo.setCoordinate(17, new Point3d(-AX, AY, 0));
        //linea der
        liCampo.setCoordinate(18, new Point3d(-X, -AY, 0));
        liCampo.setCoordinate(19, new Point3d(-AX, -AY, 0));
        //liea arriba
        liCampo.setCoordinate(20, new Point3d(-AX, AY, 0));
        liCampo.setCoordinate(21, new Point3d(-AX, -AY, 0));
        
                
        /*area Menor 1*/
        //linea izq
        liCampo.setCoordinate(22, new Point3d(X, AY_M, 0));
        liCampo.setCoordinate(23, new Point3d(AX_M, AY_M, 0));
        //linea der
        liCampo.setCoordinate(24, new Point3d(X, -AY_M, 0));
        liCampo.setCoordinate(25, new Point3d(AX_M, -AY_M, 0));
        //liea arriba
        liCampo.setCoordinate(26, new Point3d(AX_M, AY_M, 0));
        liCampo.setCoordinate(27, new Point3d(AX_M, -AY_M, 0));
        
        /*area Menor 2*/
        //linea izq
        liCampo.setCoordinate(28, new Point3d(-X, AY_M, 0));
        liCampo.setCoordinate(29, new Point3d(-AX_M, AY_M, 0));
        //linea der
        liCampo.setCoordinate(30, new Point3d(-X, -AY_M, 0));
        liCampo.setCoordinate(31, new Point3d(-AX_M, -AY_M, 0));
        //liea arriba
        liCampo.setCoordinate(32, new Point3d(-AX_M, AY_M, 0));
        liCampo.setCoordinate(33, new Point3d(-AX_M, -AY_M, 0));
        
        return new Shape3D(liCampo, appL);
   }
   private Texture cargarTextura(String textura)
   {
        URL url = getClass().getResource("/images/"+textura);
        TextureLoader loader = new TextureLoader(url, null);
        return loader.getTexture();
   }
}
