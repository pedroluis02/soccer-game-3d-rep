/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gamesoccer3d;

import com.sun.j3d.utils.universe.SimpleUniverse;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.j3d.Canvas3D;
import javax.swing.*;

/**
 *
 * @author Pedro Luis
 */
public class GameSoccer3D extends JFrame
{
    /**
     * @param args the command line arguments
     */
    private SimpleUniverse simpleUniverse;
    private Canvas3D canvas3D;
    private Escena3DGame escena3DGame;
    public GameSoccer3D()
    {
        super("Game Soccer 3D");
        canvas3D = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
        Dimension dim = getSizeScreen();
        Box box = Box.createHorizontalBox();
        box.setBorder(BorderFactory.createEtchedBorder());
        box.add(canvas3D, Box.CENTER_ALIGNMENT);
        add(box, BorderLayout.CENTER);
        escena3DGame = new Escena3DGame();
        add(escena3DGame.getToolBarNorth(), BorderLayout.NORTH);
        simpleUniverse = new SimpleUniverse(canvas3D);
        simpleUniverse.addBranchGraph(escena3DGame);
        simpleUniverse.getViewingPlatform().setNominalViewingTransform();
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent we){
                System.exit(0);
            }
        });
        setSize(dim.width, dim.height);
        setVisible(true);
    }
    public static Dimension getSizeScreen()
    {
        return Toolkit.getDefaultToolkit().getScreenSize();
    }
    public static void main(String[] args) 
    {
        //TODO code application logic here
        /*boolean success = false;
        try{
            for(UIManager.LookAndFeelInfo info: UIManager.getInstalledLookAndFeels()){
                if(info.getName().compareTo("Nimbus") == 0){
                    UIManager.setLookAndFeel(info.getClassName());
                    success = true;
                    break;
                }
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Error la cargar tema",
                    "Cargador de tema", JOptionPane.WARNING_MESSAGE);
            success = false;
        }
        if(!success)*/
            JFrame.setDefaultLookAndFeelDecorated(true);
        GameSoccer3D game3d = new GameSoccer3D();
//        game3d.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }
}