/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gamesoccer3d;

import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import objetos.Campo;
import com.sun.j3d.utils.behaviors.mouse.MouseRotate;

import java.awt.AWTEvent;
import java.awt.Color;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import javax.media.j3d.*;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.plaf.BorderUIResource;

import javax.vecmath.*;
import objetos.Balon;
import objetos.Equipo;
import objetos.Jugada;
import objetos.Jugador;

/**
 *
 * @author Pedro Luis
 */
public class Escena3DGame extends BranchGroup implements ActionListener
{
    private TransformGroup transformGroup;
    private Transform3D transform3D;
    private MouseRotate mouseRotate;
    private Campo campo;
    private Balon balon;
    private Equipo equipo1;
    private Equipo equipo2;
    ///////////////////
    /*---------------------------------------*/
    private JToolBar toolBarNorth;
    /*---------------------------------------*/
    private JToggleButton botonZoomPlus, botonZoomMinus;
    private JToggleButton botonRight, botonLeft;
    private JToggleButton botonResetV, botonExit;
    private JComboBox  jugadas;
    /*---------------------------------------*/
    private double vistaXN, vistaYN, vistaZN;
    private double vistaXP, vistaYP, vistaZP;
    public Escena3DGame()
    { 
        vistaXP = 10.5; vistaYP =  5; vistaZP =  5; 
        vistaXN = -6; vistaYN = -5; vistaZN = -5; 
        campo = new Campo(); 
        balon = new Balon(0.08f);
        
        transform3D = new Transform3D();
        transform3D.ortho(-10, 15, -6, 10, -10, 10);
        transform3D.lookAt(new Point3d(vistaXP, vistaYP, vistaZP), 
                new Point3d(vistaXN, vistaYN, vistaZN), new Vector3d(0, 0, vistaZP));
        transformGroup = new TransformGroup(transform3D);
        transformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        transformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
//        transformGroup.addChild(crearEjes());
        transformGroup.addChild(campo);
        transformGroup.addChild(balon);
        /////
        Color3f h1 = new Color3f(Color.yellow);
        Color3f s1 = new Color3f(Color.blue);
        equipo1 = new Equipo(transformGroup, h1, s1, 10, 1);
        ////////
        Color3f h2 = new Color3f(Color.red);
        Color3f s2 = new Color3f(Color.white);
        equipo2 = new Equipo(transformGroup, h2, s2, 10, 2);
        //////
        addChild(transformGroup);
        mouseRotate = new MouseRotate(transformGroup);
        mouseRotate.setSchedulingBounds(new BoundingSphere());
        transformGroup.addChild(mouseRotate);
//        ///
        Jugada j = new Jugada(equipo1, balon);
        j.setSchedulingBounds(new BoundingSphere());
        transformGroup.addChild(j);
        //
        compile();
    }
    private TransformGroup crearEjes()
    {
        Transform3D tdVista = new Transform3D();
        tdVista.setTranslation(new Vector3d(0, 0, 0));
        TransformGroup ejes = new TransformGroup(tdVista);
        LineArray ejeX = new LineArray(2, LineArray.COORDINATES | LineArray.COLOR_3);
        LineArray ejeY = new LineArray(2, LineArray.COORDINATES | LineArray.COLOR_3);
        LineArray ejeZ = new LineArray(2, LineArray.COORDINATES | LineArray.COLOR_3);
        
        ejeX.setCoordinate(0, new Point3d(0, 0,0));
        ejeX.setCoordinate(1, new Point3d(1, 0, 0));
        
        ejeY.setCoordinate(0, new Point3d(0, 0,0));
        ejeY.setCoordinate(1, new Point3d(0, 1, 0));
        
        ejeZ.setCoordinate(0, new Point3d(0, 0,0));
        ejeZ.setCoordinate(1, new Point3d(0, 0, 1));
        
        ejeX.setColor(0, new Color3b(Color.red));
        ejeY.setColor(0, new Color3b(Color.blue));
        ejeZ.setColor(0, new Color3b(Color.green));
        
        LineAttributes la = new LineAttributes();
        la.setLineWidth(4.0f);
        
        Appearance app = new Appearance();
        app.setLineAttributes(la);
        
        ejes.addChild(new Shape3D(ejeX, app));
        ejes.addChild(new Shape3D(ejeY, app));
        ejes.addChild(new Shape3D(ejeZ, app));
        
        return ejes;
    }
    public JToolBar getToolBarNorth()
    {
        toolBarNorth = new JToolBar(JToolBar.HORIZONTAL);
        toolBarNorth.setFloatable(false);
        toolBarNorth.setLayout(new BoxLayout(toolBarNorth, BoxLayout.X_AXIS));
        //
        botonZoomPlus = new JToggleButton(
                new ImageIcon(getClass().getResource("/icons/zoom_in.png")));
        botonZoomMinus = new JToggleButton(
                new ImageIcon(getClass().getResource("/icons/zoom_out.png")));
        botonResetV = new JToggleButton(
                new ImageIcon(getClass().getResource("/icons/reset.png")));
        
        botonZoomPlus.setPreferredSize(new Dimension(40, 30));
        botonZoomMinus.setPreferredSize(new Dimension(40, 30));
        botonResetV.setPreferredSize(new Dimension(40, 30));
        
        botonZoomPlus.setBorder(BorderFactory.createRaisedBevelBorder());
        botonZoomMinus.setBorder(BorderFactory.createRaisedBevelBorder());
        botonResetV.setBorder(BorderFactory.createRaisedBevelBorder());
        
        botonZoomPlus.setToolTipText("Acercar Vista");
        botonZoomMinus.setToolTipText("Alejar Vista");
        botonResetV.setToolTipText("Reinicia la Vista");
        
        botonZoomPlus.setActionCommand("plus");
        botonZoomMinus.setActionCommand("minus");
        botonResetV.setActionCommand("reset");
        
        botonZoomPlus.addActionListener(this);
        botonZoomMinus.addActionListener(this);
        botonResetV.addActionListener(this);
               
        ///
        botonLeft = new JToggleButton(
                new ImageIcon(getClass().getResource("/icons/izq.gif")));
        botonRight = new JToggleButton(
                new ImageIcon(getClass().getResource("/icons/der.gif")));
        botonExit = new JToggleButton(
                new ImageIcon(getClass().getResource("/icons/exit.png")));
        
        botonRight.setPreferredSize(new Dimension(40, 30));
        botonLeft.setPreferredSize(new Dimension(40, 30));
        botonExit.setPreferredSize(new Dimension(40, 30));
        
        botonRight.setToolTipText("Mover vista hacia la derecha");
        botonLeft.setToolTipText("Mover vista hacia la izquierda");
        botonExit.setToolTipText("Salir de la simulación");
        
        botonRight.setBorder(BorderFactory.createRaisedBevelBorder());
        botonLeft.setBorder(BorderFactory.createRaisedBevelBorder());
        botonExit.setBorder(BorderFactory.createRaisedBevelBorder());
        
        botonLeft.setActionCommand("izq");
        botonRight.setActionCommand("der");
        botonExit.setActionCommand("exit");
        
        botonLeft.addActionListener(this);
        botonRight.addActionListener(this);
        botonExit.addActionListener(this);
       
        ///
        jugadas = new JComboBox();
        jugadas.addActionListener(this);
        
        jugadas.addItem("Jugada 1 ");
        jugadas.addItem("Jugada 2 ");
        jugadas.addItem("Jugada 3 ");
        ///
        toolBarNorth.add(jugadas);
        toolBarNorth.addSeparator();
        toolBarNorth.addSeparator();
        toolBarNorth.add(botonZoomPlus);
        toolBarNorth.addSeparator();
        toolBarNorth.add(botonZoomMinus);
        toolBarNorth.addSeparator();
        toolBarNorth.addSeparator();
        toolBarNorth.add(botonLeft);
        toolBarNorth.addSeparator();
        toolBarNorth.add(botonRight);
        toolBarNorth.addSeparator();
        toolBarNorth.addSeparator();
        toolBarNorth.add(botonResetV);
        toolBarNorth.addSeparator();
        toolBarNorth.add(botonExit);
        
        return toolBarNorth;
    }
    private void resetVista()
    {
        vistaXP = 10.5; vistaYP =  5; vistaZP =  5; 
        vistaXN = -6; vistaYN = -5; vistaZN = -5; 
        transform3D.ortho(-10, 15, -6, 10, -10, 10);
        transform3D.lookAt(new Point3d(vistaXP, vistaXP, vistaZP), 
                new Point3d(vistaXN, vistaYN, vistaZN), 
                new Vector3d(0, 0, vistaZP));
        transformGroup.setTransform(transform3D);
    }
    @Override
    public void actionPerformed(ActionEvent e)
    {
        String com = e.getActionCommand();
        int op = jugadas.getSelectedIndex();
        if(com.compareTo("plus")==0)
        {
           if(vistaYP==2)
             return;
           vistaXP-=0.2; vistaYP-=0.2;   
            vistaZP+=0.2; 
        }
        else if(com.compareTo("minus")==0)
        {
           if(vistaXP==13)
             return;
           vistaXP+=0.2; vistaYP+=0.2; 
        }
        else if(com.compareTo("der")==0)
        {
            vistaXP+=0.2; vistaYP-=0.2; 
        }
        else if(com.compareTo("izq")==0)
        {
            vistaXP-=0.2; vistaYP+=0.2; 
        }
        else if(com.compareTo("reset") == 0){
            resetVista();
        }
        else if(com.compareTo("exit") == 0){
            System.exit(0);
        }
        ////////////////////////////////////////////////
        if(op==0)
        {
            
        }
        else if(op==1)
        {
            
        }
        else if(op==2)
        {
            
        }
        
        if(com.compareTo("comboBoxChanged")!=0)
        {
            transform3D.ortho(-10, 15, -6, 10, -10, 10);
            transform3D.lookAt(new Point3d(vistaXP, vistaYP, vistaZP), 
                    new Point3d(vistaXN, vistaYN, vistaZN),
                    new Vector3d(0, 0, vistaZP)); 
            transformGroup.setTransform(transform3D);
        }
    }
}
