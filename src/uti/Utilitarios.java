package uti;

import com.sun.j3d.utils.image.TextureLoader;
import java.net.URL;
import javax.media.j3d.Texture;

public class Utilitarios
{
    public static final double MATRIZ_BEZIER[][]= 
    {
        {-1,  3, -3, 1},
        {3, -6,  3, 0},
        {-3,  3,  0, 0},
        {1,  0,  0, 0} 
    };
 	public static double[][] multiplicarMatrices(double[][] A, double[][] B)
 	{
            int i=0,j=0, k=0, fA=0, cA=0, fB=0, cB=0;
            double[][] C=null;

            fA=A.length;
            fB=B.length;

            cA=A[0].length;
            cB=B[0].length;
// 		System.out.print(fA+"  " +fB+" "+cA+"   "+cB);
            if (cA==fB)
            {
                C=new double[fA][cB];
                for (i=0;i<fA;i++)
                {
                    for (j=0;j<cB;j++)
                    {
                        C[i][j]=0;
                        for(k=0;k<cA;k++)
                        {
                                C[i][j]=C[i][j]+ A[i][k]*B[k][j];
                        }

                    }

                }
            }

            return C;
 	}
        public static double[][] productoporEscalar(double escalar, double[][] matriz)
        {
            int fila = matriz.length, col = matriz[0].length;
            double[][] aux = new double[fila][col];
            for(int i=0; i<fila; i++)
            {
                for(int j=0; j<col; j++) 
                    aux[i][j] = matriz[i][j]*(escalar);
            }
            return aux;

        }

 	/*Permite mostrar los valores de una matriz*/
 	public static void reportarMatriz(double[][] matriz)
 	{
            int i=0;
            int j=0;

            for(i=0;i<matriz.length;i++)
            {
                for(j=0;j<matriz[i].length;j++)
                {
                        System.out.print(matriz[i][j]+"  ") ;
                }
                System.out.println();
            }

 	}

 	/***/
 	public static void reportarArreglo(double[] arreglo)
 	{
 		int i=0;
 		for (i=0;i<arreglo.length;i++)
 		{
 			System.out.println(arreglo[i]+" ");
 		}

 	}

 	public static void reportarArreglo(int[] arreglo)
 	{
 		int i=0;
 		for (i=0;i<arreglo.length;i++)
 		{
 			System.out.print(arreglo[i]+" ");
 		}

 	}
 	public static void pausa()
 	{

            try
            {
                System.in.read();
            }
            catch(Exception error) 
            {
                error.printStackTrace();
            }
	}

	/*multiplicacio vectorial  Ay B son  vectores en R3 */
	public static double[] obtenerProductoCruz(double[] A, double[] B)
	{
		double[] prod= new double[3];

		prod[0] = A[1]*B[2] - B[1]*A[2];
		prod[1] = A[2]*B[0] - B[2]*A[0];
		prod[2] = A[0]*B[1] - B[0]*A[1];
		return prod;
	}


 	/*producto punto Ay B son  vectores en R3 */
	public static double obtenerProductoPunto(double[] A, double[] B)
	{
		double prod= 0;

		prod = A[0]*B[0] + A[1]*B[1]+ A[2]*B[2];
		return prod;
	}

	/*producto de un escalar por un vector */
	public static double[] obtenerProducto(double A, double[] B)
	{
		double[] prod= new double[3];

		prod[0] = A*B[0];
		prod[1] = A*B[1];
		prod[2] = A*B[2];

		return prod;
	}

	/*sunmar Ay B son  vectores en R3 */
	public static double[] obtenerSuma(double[] A, double[] B)
	{
		double[] suma= new double[3];

		suma[0] = A[0]+B[0];
		suma[1] = A[1]+B[1];
		suma[2] = A[2]+B[2];
		return suma;
	}

	/*restar Ay B son  vectores en R3 */
	public static double[] obtenerResta(double[] A, double[] B)
	{
		double[] resta= new double[3];

		resta[0] = A[0]-B[0];
		resta[1] = A[1]-B[1];
		resta[2] = A[2]-B[2];
		return resta;
	}


	/*obtener vector unitario*/
	public static double[] obtenerUnitario(double[] A)
	{
		double norma= 0;
		double[] unitario = new double[3];

		norma=Math.sqrt(A[0]*A[0] + A[1]*A[1]+ A[2]*A[2]);

		unitario[0] = A[0]/norma;
		unitario[1] = A[1]/norma;
		unitario[2] = A[2]/norma;
		return unitario;
	}
 }
