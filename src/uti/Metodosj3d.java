/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uti;

import java.util.ArrayList;
import javax.media.j3d.PointArray;
import javax.vecmath.Point3d;

/**
 *
 * @author Pedro Luis
 */
public class Metodosj3d 
{
    public static final int INFERIOR = -1;
    public static final int SUPERIOR  = 1;
    public static final int TODO = 0;
    
    public static final double MATRIZ_BEZIER[][]= 
    {
        {-1,  3, -3, 1},
        {3, -6,  3, 0},
        {-3,  3,  0, 0},
        {1,  0,  0, 0} 
    };
    
    public static PointArray calcularPuntosCirculo(double x_c, double y_c, double z_c, double r, int op)
    {
        ArrayList <Point3d> puntos = new ArrayList<Point3d>();
        double d0, x, y;
        d0 = 1 - r;
        x = 0;
        y = r;
        puntos.add(new Point3d(x_c, y_c + r, z_c));
        puntos.add(new Point3d(x_c, y_c - r, z_c));
        puntos.add(new Point3d(x_c + r, y_c, z_c));
        puntos.add(new Point3d(x_c - r, y_c, z_c));
        for(x=0.0; x<y; x+=0.01)
        {
            if(d0 < 0)
               d0 += 2*x;
            else
            {
                y-=0.01;
                d0 += 2*(x - y);
            }
            //inferior
            if(op==INFERIOR || op==TODO)
            {
                puntos.add(new Point3d(x_c + x, y_c + y, z_c));
                puntos.add(new Point3d(x_c + x, y_c - y, z_c));
                puntos.add(new Point3d(x_c + y, y_c + x, z_c));
                puntos.add(new Point3d(x_c + y, y_c - x, z_c));
            }
            //superior
            if(op==SUPERIOR  || op==TODO)
            {
                puntos.add(new Point3d(x_c - x, y_c + y, z_c));
                puntos.add(new Point3d(x_c - x, y_c - y, z_c));
                puntos.add(new Point3d(x_c - y, y_c + x, z_c));
                puntos.add(new Point3d(x_c - y, y_c - x, z_c));
            }
        }
        //System.out.println("Size: "+puntos.size());
        PointArray pa = new PointArray(puntos.size(), PointArray.COORDINATES);
        for(int i=0; i<puntos.size(); i++)
            pa.setCoordinate(i, puntos.get(i));
        return pa;        
    }
    public static PointArray surfaceBezier(double[][] puntosX, double[][] puntosY, double[][] puntosZ)
    {
        PointArray pa = new PointArray(2500, PointArray.COORDINATES);
        int contador = 0;
        double[][] U, V, R1, RX1, RX2, RY1, RY2, RZ1, RZ2, pX, pY, pZ;
        for(double u=0; u<=1; u+=0.02)
        {
            U = new double[][]{{u*u*u, u*u, u, 1}};
            for(double v=0; v<=1; v+=0.02)
            {
                V = new double[][]{{v*v*v}, {v*v}, {v}, {1}};
                R1 = Utilitarios.multiplicarMatrices(U, MATRIZ_BEZIER);
                
                RX1 = Utilitarios.multiplicarMatrices(R1, puntosX);
                RY1 = Utilitarios.multiplicarMatrices(R1, puntosY);
                RZ1 = Utilitarios.multiplicarMatrices(R1, puntosZ);
                
                RX2 = Utilitarios.multiplicarMatrices(RX1, MATRIZ_BEZIER);
                RY2 = Utilitarios.multiplicarMatrices(RY1, MATRIZ_BEZIER);
                RZ2 = Utilitarios.multiplicarMatrices(RZ1, MATRIZ_BEZIER);
                
                pX = Utilitarios.multiplicarMatrices(RX2, V);
                pY = Utilitarios.multiplicarMatrices(RY2, V);
                pZ = Utilitarios.multiplicarMatrices(RZ2, V);
                pa.setCoordinate(contador, 
                        new Point3d(pX[0][0], pY[0][0], pZ[0][0]));
                contador++;
                
            }
        }
        return pa;
    }
}
