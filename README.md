# Soccer Game 3D Representation

A soccer game 3D repesentation with players simulations

IDE
-----
Netbeans IDE

Libraries
---------
* Java 3D

Install Java 3D: 1.5.1
----------------------
* [Java 3D Info](https://www.oracle.com/technetwork/java/javase/tech/index-jsp-138252.html)
* [Download Java 3D](https://www.oracle.com/java/technologies/java-archive-downloads-java-client-downloads.html#java3d-1.5.1-oth-JPR)
